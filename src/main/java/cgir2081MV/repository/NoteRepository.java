package cgir2081MV.repository;

import cgir2081MV.main.ClasaException;
import cgir2081MV.model.Nota;

import java.util.List;

public interface NoteRepository {
	
	public void addNota(Nota nota) throws ClasaException;
	public List<Nota> getNote(); 
	public void readNote(String fisier);
	
}
