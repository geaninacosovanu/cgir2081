package cgir2081MV.repository;

import cgir2081MV.main.ClasaException;
import cgir2081MV.model.Corigent;
import cgir2081MV.model.Elev;
import cgir2081MV.model.Medie;
import cgir2081MV.model.Nota;

import java.util.HashMap;
import java.util.List;

public interface ClasaRepository {
	
	public void creazaClasa(List<Elev> elevi, List<Nota> note);
	public HashMap<Elev, HashMap<String, List<Double>>> getClasa();
	public List<Medie> calculeazaMedii() throws ClasaException;
	public void afiseazaClasa();
	public List<Corigent> getCorigenti();
}
